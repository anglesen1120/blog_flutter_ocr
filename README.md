# blog_text_recognition

A Flutter project demonstrate how to add text-recognition / OCR capability.

## Getting Started

This project acts as a starting point on implementing text-recognition / OCR capability for a Flutter app.

A few resources to get you started:

- [simple ocr plugin](https://pub.dev/packages/simple_ocr_plugin)
- [Google ML-Kit](https://developers.google.com/ml-kit/vision/text-recognition)

